#name = ["Ahmad","Tono","Tini", "Bambang", "Agus", "Agung"]
#if nama == "Ahmad"
 #s="hello"
 #b = s.reverse
 #puts "reversnya #{b}"
 names = ["Ahmad", "Tono", "Tini", "Bambang", "Agus", "Agung"]

 def check_name(name)
     if (name.length % 2 != 0) && name[0] == "A"
         puts name.upcase
     elsif name[0] == "T" 
         puts name.downcase
     else
         puts name.reverse.capitalize
     end
 end
 
 names.each do |name|
     check_name(name)
 end